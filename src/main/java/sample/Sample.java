package sample;

import com.twitter.finagle.http.Response;
import com.twitter.finatra.http.AbstractController;
import com.twitter.finatra.http.AbstractHttpServer;
import com.twitter.finatra.http.routing.HttpRouter;
import com.twitter.util.Future;

public final class Sample {

    private static final class Server extends AbstractHttpServer {

        @Override
        public void configureHttp(HttpRouter router) {
            router.add(new AbstractController() {
                @Override
                public void configureRoutes() {
                    get("/", request -> Future.apply(Sample::response));
                }
            });
        }

    }

    private static Response response() {
        Response r = new Response.Ok();
        r.setContentString("hello, world");
        return r;
    }

    public static void main(String[] args) {
        new Server().main(args);
    }

}
